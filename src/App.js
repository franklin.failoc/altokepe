import { BrowserRouter as Router, Route } from 'react-router-dom';
import './App.scss';
import React, { Component } from "react";
import Sidebar from './components/Sidebar';
import Navbar from '../src/components/Navbar';
import Historial from './pages/Historial';
import Ventas from './pages/Ventas';
import Cuenta from './pages/Cuenta';
import Clientes from './pages/Clientes';
import Ubicacion from './pages/Ubicacion';
import Empleados from './pages/Empleados';

function App() {
  return (
    <Router>
      <Navbar/>
      <div className="flex">
        <Sidebar />
        <div className="content">
          <Route path="/" exact={true} component={Cuenta} />

          <Route path="/ventas" exact={true} component={Ventas} />

          <Route path="/historial" exact={true} component={Historial} />

          <Route path="/clientes" exact={true} component={Clientes} />

          <Route path="/ubicacion" exact={true} component={Ubicacion} />

          <Route path="/empleados" exact={true} component={Empleados} />
        </div>
      </div>
    </Router>
  );
}

export default App;
