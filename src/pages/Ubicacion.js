import React, {useState, useEffect} from 'react';
import MaterialTable from 'material-table';
import axios from 'axios';

const columnas=[
    {title:'Nombres',field:'nombres'},
    {title:'Apellidos',field:'apellidos'},
    {title:'Dni',field:'dni', type: "numeric"},
    {title:'Celular',field:'celular', type: "numeric"},
    {title:'Correo',field:'correo'},
    {title:'Ocupacion',field:'ocupacion'},
    {title:'Sueldo',field:'sueldo', type: "numeric"},
    {title:'Inicia',field:'fechainicio', type: "numeric"},
    {title:'Finaliza',field:'fechafin', type: "numeric"}
];
const baseurl='http://localhost:8080/altokePe/empleados/listar/1';

function Ubicacion() {
    
    const [data, setData]= useState([]);
    const peticionGet=async()=>{
        await axios.get(baseurl)
        .then(response=>{
            setData(response.data);
        })
    }

    useEffect(()=>{
        peticionGet();
    }, [])
    


    return(
        <div >
            <MaterialTable 
                columns={columnas}
                data={data}
                title="Empleados Registrados"
                actions={[{    
                    icon: 'edit',
                    tooltip: 'Editar Artista',
                    onClick: (event, rowData)=>alert("Has presionado editar: "+ rowData.artista)
                },
                {    
                    icon: 'delete',
                    tooltip: 'Eliminar Artista',
                    onClick: (event, rowData)=>window.confirm("Estas seguro de eliminar al artista: "+ rowData.artista+'?')
                }]}
                options={{
                    actionsColumnIndex: -1
                }}
                localization={{
                    header:{
                        actions: 'Acciones'
                    }
                }}
            />
        </div>
    );
}

export default Ubicacion;