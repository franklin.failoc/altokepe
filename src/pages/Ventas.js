import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import {
  Table,
  Button,
  Container,
  Modal,
  ModalHeader,
  ModalBody,
  FormGroup,
  ModalFooter,
} from "reactstrap";



class Ventas extends Component{

        

    render() {
        if (this.state.recuperado)
          return this.mostrarTabla()
        else
          return (<div>recuperando datos...</div>)
      }   
      
      
      constructor(props) {
        super(props);
        this.state = {
            productos: [],
            recuperado: false,
            modalInsertar: false,
            modalEditar: false,
            modalEliminar: false,
            form:{
                id: '',
                nombre: '',
                descripcion: '',
                precio: '',
            },
            
        }
      }

        handleChange=prod=>{
        this.setState({
            form:{
                ...this.state.form,
            [prod.target.name]: prod.target.value,
            }
        });
        }

        //------LOGICA PARA INSERTAR DATOS------//
        mostrarModalInsertar=()=>{
          this.setState({modalInsertar: true});
        }

        ocultarModalInsertar=()=>{
        this.setState({modalInsertar: false});
        }

        insertar=()=>{
            var valorNuevo={...this.state.form};
            valorNuevo.id=this.state.productos.length+1;
            var lista=this.state.productos;
            lista.push(valorNuevo);
            this.setState({productos: lista, modalInsertar: false});
        }

        //------LOGICA PARA EDITAR DATOS------//
        mostrarModalEditar=(registro)=>{
            this.setState({modalEditar: true, form: registro});
        }
  
        ocultarModalEditar=()=>{
          this.setState({modalEditar: false});
        }

        

        editar=(dato)=>{
            var contador=0;
            var lista=this.state.productos;
            lista.map((registro)=>{
                if(dato.id==registro.id){
                    lista[contador].nombre=dato.nombre;
                    lista[contador].descripcion=dato.descripcion;
                    lista[contador].precio=dato.precio;
                }
                contador++;
            });
            this.setState({productos: lista, modalEditar: false});
        }

        //------LOGICA PARA ELIMINAR DATOS------//

        ocultarModalEliminar=()=>{
            this.setState({modalEliminar: false});
        };

        
        eliminar=(dato)=>{
            var opcion=window.confirm("¿Realmente desea eliminar el registro? " +dato.id);
            if(opcion == true){
                var contador=0;
                var lista = this.state.productos;
                lista.map((registro)=>{
                    if(dato.id == registro.id){
                        lista.splice(contador, 1);
                    }
                    contador++;
                });
            this.setState({productos: lista, modalEliminar: false});
            }
        };   
      
        componentWillMount() {
        fetch('http://127.0.0.1:8000/producto/')
          .then((response) => {
            return response.json()
          })
          .then((prod) => {
            this.setState({ productos: prod, recuperado: true, modalActualizar: false,
                modalInsertar: false})
          })    
        } 
      
      
        mostrarTabla() {
        return (
            <>

                <Container>
                    <br />
                    <Button color="success" onClick={()=>this.mostrarModalInsertar()}>Crear</Button>
                    <br />
                    <br />
                    <Table>
                        <thead>
                            <tr>
                                <th>Código</th>
                                <th>Nombre</th>
                                <th>Descripción</th>
                                <th>Precio</th>
                                <th>Accion</th>
                            </tr>
                        </thead>

                        <tbody>
                            {this.state.productos.map((prod)=>(
                            
                            <tr key={ prod.id}>
                                <td>{ prod.id}</td>
                                <td>{ prod.nombre}</td>
                                <td>{ prod.descripcion}</td>
                                <td>{ prod.precio}</td>
                                <td>
                                <Button
                                    color="primary" onClick={()=>this.mostrarModalEditar(prod)}>Editar</Button>
                                    {" "}
                                <Button color="danger" onClick={()=>this.eliminar(prod)}>Eliminar</Button>
                                </td>
                            </tr>
                            ))}
                        </tbody>
                    </Table>
                </Container>


                <Modal isOpen={this.state.modalInsertar}>
                    <ModalHeader>
                        <div>
                            <h3>Insertar registro</h3>
                        </div>
                    </ModalHeader>

                    <ModalBody>
                        <FormGroup>
                            <label>Id</label>
                            <input className="form-control" readOnly  type="text" value={this.state.productos.length+1}/>
                        </FormGroup>

                        <FormGroup>
                            <label>Nombre</label>
                            <input className="form-control" name="nombre"  type="text" onChange={this.handleChange}/>
                        </FormGroup>

                        <FormGroup>
                            <label>Descripción</label>
                            <input className="form-control" name="descripcion"  type="text" onChange={this.handleChange}/>
                        </FormGroup>

                        <FormGroup>
                            <label>Precio</label>
                            <input className="form-control" name="precio"  type="text" onChange={this.handleChange}/>
                        </FormGroup>
                    </ModalBody>


                    <ModalFooter>
                        <Button color="primary" onClick={()=>this.insertar()}>Insertar</Button>
                        <Button color="danger" onClick={()=>this.ocultarModalInsertar()}>Cancelar</Button>
                    </ModalFooter>
                </Modal>

                <Modal isOpen={this.state.modalEditar}>
                    <ModalHeader>
                        <div>
                            <h3>Editar registro</h3>
                        </div>
                    </ModalHeader>

                    <ModalBody>
                        <FormGroup>
                            <label>Id</label>
                            <input className="form-control" readOnly  type="text" value={this.state.form.id} />
                        </FormGroup>

                        <FormGroup>
                            <label>Nombre</label>
                            <input className="form-control" name="nombre"  type="text" onChange={this.handleChange} value={this.state.form.nombre}/>
                        </FormGroup>

                        <FormGroup>
                            <label>Descripción</label>
                            <input className="form-control" name="descripcion"  type="text" onChange={this.handleChange} value={this.state.form.descripcion}/>
                        </FormGroup>

                        <FormGroup>
                            <label>Precio</label>
                            <input className="form-control" name="precio"  type="text" onChange={this.handleChange} value={this.state.form.precio}/>
                        </FormGroup>
                    </ModalBody>


                    <ModalFooter>
                        <Button color="primary" onClick={()=>this.editar(this.state.form)}>Editar</Button>
                        <Button color="danger" onClick={()=>this.ocultarModalEditar()}>Cancelar</Button>
                    </ModalFooter>
                </Modal>
           </>                 
            
      
        )
    }

}

export default Ventas;